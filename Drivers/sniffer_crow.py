import serial
import time
import os
import json
from serial.serialutil import SerialException
from Logger import TestLogger


# TIMEOUT_READ = 30

# DEVICES_SN = {
#     16777215: 'BROADCAST',  # FFFFFF
#     900053: 'CAM',
#     2146147: 'CAM2',
#     1587497: 'AIR',
#     1583195: 'Igor panel',
#     1868227: 'Tanya panel',
# }


def config_mode(func):
    def wrap(self, *argc, **kwargs):
        self.enter_config()
        res = func(self, *argc, **kwargs)
        self.exit_config()
        return res
    return wrap


class SnifferCrow(object):
    # ##################################################################################
    # ###                       Init / close region                                  ###
    # ##################################################################################

    def __init__(self, port, serial_number, logger):
        """Constructor: port is a COM port address in string """
        self.port = port
        self.logger = logger
        self.sSerial = serial.Serial()
        self.serial_number = self.convert_sn2hex_rev(serial_number)
        self.config = SnifferCrow.read_config_data()

    def start(self):
        """Init and open serial port.
           return: True or False"""
        self.sSerial.port = self.port
        self.sSerial.baudrate = 115200
        self.sSerial.timeout = 0.5

        try:
            self.sSerial.open()
        except SerialException, err:
            self.sSerial.close()
            raise
        return self.sSerial.isOpen()

    def stop(self):
        """Close the port"""
        self.sSerial.close()
        msg = 'Sniffer COM-Port {} is closed'.format(self.port)
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})

    def is_open(self):
        return self.sSerial.isOpen()

    @staticmethod
    def reverse_sn_hex(sn):
        """
        Prepares serial number for set sn command in reverse order by byte
        :param sn: serial number hex string
        :type sn: str
        :return: serial number in reverse order
        :rtype: str
        """
        lst = [sn[i:i+2] for i in range(0, len(sn), 2)]
        rev_lst = lst[::-1]
        return "".join(rev_lst)

    @staticmethod
    def read_config_data():
        working_dir = os.getcwd()
        config_file = os.path.join(working_dir, 'config.json')
        with open(config_file) as data_file:
            return json.load(data_file)

    def convert_sn2hex_rev(self, sn_dec):
        """
        Convert and set decimal SN to HEX upper representation without leading '0x'
        :param sn_dec: decimal serial number; int
        """
        if sn_dec is not None:
            sn_hex = '{:06x}'.format(sn_dec).upper()
            sn_hex_rev = SnifferCrow.reverse_sn_hex(sn_hex)
            # self.logger.debug('SN hex reversed: {}'.format(sn_hex_rev))
        else:
            sn_hex_rev = None

        return sn_hex_rev

    def convert_hex_rev2sn(self, sn_hex_rev):
        """
        Convert and set decimal SN to HEX upper representation without leading '0x'
        :param sn_dec: decimal serial number; int
        """
        if sn_hex_rev is not None:
            sn_hex = SnifferCrow.reverse_sn_hex(sn_hex_rev)
            sn_dec = int(sn_hex, 16)
            # self.logger.debug('SN dec: {}'.format(sn_dec))
        else:
            sn_dec = None

        return sn_dec

    # ##################################################################################
    # ###                           Public functions                                 ###
    # ##################################################################################

    def enter_config(self):
        self.sSerial.write(':CONFIG;')

    def exit_config(self):
        self.sSerial.write(':EXIT;')

    def set_filter1_cam2all(self, *args, **kwargs):
        msg = 'Set FILTER 1'
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':FILTER:1:ON:{}:000000:00;'.format(self.serial_number))
        time.sleep(0.1)

    def set_filter2_all2cam(self, *args, **kwargs):
        msg = 'Set FILTER 2'
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':FILTER:2:ON:000000:{}:00;'.format(self.serial_number))
        time.sleep(0.1)

    def set_filter3_cam2broadcast(self, *args, **kwargs):
        msg = 'Set FILTER 3'
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':FILTER:3:ON:{}:FFFFFF:00;'.format(self.serial_number))
        time.sleep(0.1)

    def switch_filter1(self, *args, **kwargs):
        mode = kwargs['filter1']
        msg = 'FILTER 1 {}'.format(mode)
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':FILTER:1:{};'.format(mode))
        time.sleep(0.1)

    def switch_filter2(self, *args, **kwargs):
        mode = kwargs['filter2']
        msg = 'FILTER 2 {}'.format(mode)
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':FILTER:2:{};'.format(mode))
        time.sleep(0.1)

    def switch_filter3(self, *args, **kwargs):
        mode = kwargs['filter3']
        msg = 'FILTER 3 {}'.format(mode)
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':FILTER:3:{};'.format(mode))
        time.sleep(0.1)

    # @config_mode
    def set_channel(self, *args, **kwargs):
        frequency = kwargs['frequency']
        msg = 'Set CHANNEL {}'.format(frequency)
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':CHANNEL:{};'.format(frequency))
        time.sleep(0.1)

    def set_data_packets(self, *args, **kwargs):
        mode = kwargs['data_packets']
        msg = 'Set DATA PACKETS {}'.format(mode)
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':DATAPACKETS:{};'.format(mode))
        time.sleep(0.1)

    def set_error_packets(self, *args, **kwargs):
        mode = kwargs['error_packets']
        msg = 'Set ERROR PACKETS {}'.format(mode)
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})
        self.sSerial.write(':ERRORPACKETS:{};'.format(mode))
        time.sleep(0.1)

    @config_mode
    def configure_sniffer(self, config_funcs, *args, **kwargs):
        for f_name in config_funcs:
            f = getattr(self, f_name)
            f(*args, **kwargs)

    def clear_content(self):
        time.sleep(1)
        buf = self.sSerial.read()
        msg = 'Cleared: {}'.format(buf)
        print msg
        # self.logger.debug(msg)
        self.logger.add_row({'msg': msg})

    def read_content(self, frequency, event, break_on_found=True):
        cp_serials = set()
        sn_dec = self.convert_hex_rev2sn(self.serial_number)
        time_start = time.time()
        timeout = self.config.get('timeout_read', 60)
        devices_sn = self.config['devices_sn']
        while not event.is_set() and time.time() - time_start < timeout:
            time.sleep(timeout / 10.0)
            # time.sleep(TIMEOUT_READ / 10.0)
            lines = self.sSerial.readlines()
            msg = '{} '.format(frequency) + str(lines) if lines else '{} Empty -> PRESS-RELEASE TAMPER'.format(frequency)
            print msg
            # self.logger.debug(msg)
            self.logger.add_row({'msg': msg})
            d = {'frequency': frequency}
            device_name = str(devices_sn.get(str(sn_dec), 'Unknown'))
            for line in lines:
                lst = line.split()
                if len(lst) < 5:
                    continue
                if lst[2] == self.serial_number:
                    cp_hex = lst[3]
                    cp_sn = self.convert_hex_rev2sn(cp_hex)
                    cp_name = str(devices_sn.get(str(cp_sn), 'Unknown'))
                    d.update({'from': device_name if 'Unknown' not in device_name else sn_dec,
                              'to': cp_name if 'Unknown' not in cp_name else cp_sn,
                              'msg': line
                              })
                    print '{}'.format(d)
                    # self.logger.info(d)
                    self.logger.add_row(d)
                    if 'FFFFFF' in lst[3]:  # Broadcast
                        # d.update({'from': device_name if 'Unknown' not in device_name else sn_dec, 'To': 'FFFFFF'})
                        # print '{}'.format(d)
                        # self.logger.info(d)
                        pass
                    else:
                        cp_serials.add(cp_sn)
                        if break_on_found:
                            break
                elif lst[3] == self.serial_number:
                    cp_hex = lst[2]
                    cp_sn = self.convert_hex_rev2sn(cp_hex)
                    cp_serials.add(cp_sn)
                    cp_name = str(devices_sn.get(str(cp_sn), 'Unknown'))
                    d.update({'from': cp_name if 'Unknown' not in cp_name else cp_sn,
                              'to': device_name if 'Unknown' not in device_name else sn_dec,
                              'msg': line
                              }),

                    print '{}'.format(d)
                    # self.logger.info(d)
                    self.logger.add_row(d)
                    if break_on_found:
                        break
            if break_on_found and len(cp_serials):
                break

        return cp_serials

    def read_all_transmissions(self, frequency, timeout=None, event=None):
        cp_serials = set()
        # sn_dec = self.convert_hex_rev2sn(self.serial_number)
        time_start = time.time()
        # connections = []
        if timeout is None:
            timeout = self.config.get('timeout_read', 60)
        devices_sn = self.config['devices_sn']
        is_event_set = event.is_set() if event is not None else False
        sampling = min(timeout / 10.0, 6.0)
        while not is_event_set and time.time() - time_start < timeout:
            time.sleep(sampling)
            lines = self.sSerial.readlines()
            d = {'frequency': frequency}
            for line in lines:
                connections = []
                lst = line.split()
                if len(lst) < 5:
                    continue
                else:
                    msg = '<{}> '.format(frequency) + line.strip()
                    print msg
                    # self.logger.debug(msg)
                    connections.append(line)

                # Parse list of connections
                for connection_str in connections:
                    lst = connection_str.split()
                    src_sn = self.convert_hex_rev2sn(lst[2])
                    src_name = str(devices_sn.get(str(src_sn), 'Unknown'))
                    dest_sn = self.convert_hex_rev2sn(lst[3])
                    dest_name = str(devices_sn.get(str(dest_sn), 'Unknown'))
                    d.update({'from': src_name if 'Unknown' not in src_name else src_sn,
                              'to': dest_name if 'Unknown' not in dest_name else dest_sn,
                              'msg': connection_str
                              })
                    print '{}'.format(d), '\n'
                    # self.logger.info(d)
                    self.logger.add_row(d)

                    if lst[2] == self.serial_number and 'FFFFFF' not in lst[3]:
                        cp_serials.add(dest_sn)
                    elif lst[3] == self.serial_number:
                        cp_serials.add(src_sn)

        return cp_serials


def main():
    logger = TestLogger('Sniffer')
    sn = SnifferCrow('/dev/ttyUSB0', 1111111, logger)
    sn.start()
    print 'Port is open'
    sn.stop()
    print 'Port is closed'


if __name__ == '__main__':
    main()
