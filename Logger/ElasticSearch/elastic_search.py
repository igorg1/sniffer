from elasticsearch import Elasticsearch
from datetime import datetime
import copy


class ElasticSearchLogger(object):
    def __init__(self, index, dock_type):
        self.es = Elasticsearch(['http://elastic:changeme@localhost:9200'])
        self.index = index  # 'sniffer'
        self.doc_type = dock_type  # 'packet

    def add_row(self, msg_dict):
        row = copy.copy(msg_dict)
        row.update({'timestamp': datetime.now()})
        # row.update({'time': datetime.now()})

        self.es.index(index=self.index, doc_type=self.doc_type, body=row)