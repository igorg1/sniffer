import time
import argparse
import sys
from monitoring_connections import *
from Drivers import SnifferCrow
from Logger import TestLogger
from Logger import ElasticSearchLogger
from threading import *
from Queue import *

# COM_PORTS = [
#     '/dev/ttyUSB0',
#     '/dev/ttyUSB1',
#     '/dev/ttyUSB2',
#     '/dev/ttyUSB3',
#     '/dev/ttyUSB4',
# ]
#
# SNIFFERS_NUM2CHANNELS = {
#     1: [(868350, 869070, 868130, 868850, 868480)],
#     2: [(868350, 869070, 868130), (868850, 868480)],
#     3: [(868350, 869070), (868130,  868850), (868480,)],
#     4: [(868350, 869070,), (868130,), (868850,), (868480,)],
#     5: [(868350,), (869070,), (868130,), (868850,), (868480,)],
# }


class ContinuousMonitoring(MonitoringConnections):
    def __init__(self, logger=None):
        super(ContinuousMonitoring, self).__init__(device_sn=0, sniffers_num=5, logger=logger)

    # def monitoring(self):
    #     msg = 'Monitoring is starting...'
    #     print msg
    #     # self.logger.debug(msg)
    #     self.logger.add_row({'msg': msg})
    #
    #     cps_all_serials = []
    #     config_funcs = [
    #         'switch_filter1',
    #         'switch_filter2',
    #         'switch_filter3',
    #         'set_channel',
    #         'set_data_packets',
    #         'set_error_packets',
    #     ]
    #
    #     channels_list = SNIFFERS_NUM2CHANNELS[self.sniffers_num]
    #     queue = Queue()
    #     for idx in xrange(self.sniffers_num):
    #         thrd = Thread(target=self.config_sniffer_and_read,
    #                       args=(self.sniffers[idx], config_funcs, channels_list[idx], queue))
    #         self.threads.append(thrd)
    #         thrd.start()
    #
    #     for thrd in self.threads:
    #         thrd.join()
    #
    #     print 'Queue size: {}'.format(queue.qsize())
    #     while not queue.empty():
    #         serials_from_one_sniffer = queue.get()
    #         print 'Serial numbers from one sniffer', serials_from_one_sniffer
    #         cps_all_serials += serials_from_one_sniffer
    #
    #     return cps_all_serials

    def config_sniffer_and_read(self, sniffer, config_funcs, channels, queue):
        print 'In thread', 'Sniffer is open', sniffer.is_open()
        # cps_serials_lst = list()
        try:
            for frequency in channels:
                sniffer.configure_sniffer(config_funcs, frequency=frequency,
                                          filter1='OFF', filter2='OFF', filter3='OFF',
                                          data_packets='OFF', error_packets='OFF')
                cps_serials = sniffer.read_all_transmissions(frequency, timeout=sys.maxint, event=self.event)
        except (KeyboardInterrupt, SystemExit):
            self.event.set()


def main():
    # parser = argparse.ArgumentParser()
    # parser.add_argument('sn', help='Device SN', type=int)
    # parser.add_argument('num', help='Sniffers number', type=int)
    # # print parser
    # args = parser.parse_args()
    # # print 'Args:', args
    # device_sn = args.sn if 0 != args.sn else None
    # devices_sn = SnifferCrow.read_config_data()['devices_sn']
    # device_name = str(devices_sn.get(str(device_sn), 'Unknown'))

    # logger = TestLogger('Sniffer')
    logger = ElasticSearchLogger(index='sniffer', dock_type='packet')
    connections = ContinuousMonitoring(logger)
    cps_sn = connections.monitoring()
    # if device_sn is not None:
    #     if len(cps_sn):
    #         d_cps = {}
    #         for cp_sn in cps_sn:
    #             cp_name = str(devices_sn.get(str(cp_sn), 'Unknown'))
    #             d_cps.update({cp_sn: cp_name})
    #         msg = 'Device {} is connected to CP {}'.format({device_sn: device_name}, d_cps)
    #     else:
    #         msg = 'Device {} does not connected to any CP'.format({device_sn: device_name})
    # else:
    #     msg = ''

    connections.close_sniffers()

    # if msg:
    #     print msg
    #     # logger.debug(msg)
    #     logger.add_row({'msg': msg})


if __name__ == '__main__':
    main()
